# `/docs`

Design and user documents (in addition to your godoc generated documentation).

Examples:

* <https://github.com/gohugoio/hugo/tree/master/docs>
* <https://github.com/openshift/origin/tree/master/docs>
* <https://github.com/dapr/dapr/tree/master/docs>

Документы пользователей и дизайна (в дополнение к автоматической документации godoc).

Ознакомьтесь с директорией [`/docs`](docs/README.md) для примеров.
