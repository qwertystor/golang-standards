# `/web`

Web application specific components: static web assets, server side templates and SPAs.

Специальные компоненты для веб-приложений: статические веб-ресурсы, серверные шаблоны и одностраничные приложения.
