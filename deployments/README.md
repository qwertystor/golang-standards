# `/deployments`

IaaS, PaaS, system and container orchestration deployment configurations and templates (docker-compose, kubernetes/helm, mesos, terraform, bosh).

Шаблоны и файлы конфигураций систем оркестраций IaaS, PaaS, операционных систем и контейнеров (docker-compose, kubernetes/helm, mesos, terraform, bosh). Отметьте, что в некоторых репозиториях, особенно в приложениях, развернутых с использованием Kubernetes, эта директория называется `/deploy`.
