# `/api`

OpenAPI/Swagger specs, JSON schema files, protocol definition files.

Examples:

* https://github.com/kubernetes/kubernetes/tree/master/api
* https://github.com/moby/moby/tree/master/api

Спецификации OpenAPI/Swagger, файлы JSON schema, файлы определения протоколов.

Ознакомьтесь с директорией [`/api`](api/README.md) для примеров.
