# `/cmd`

Main applications for this project.

The directory name for each application should match the name of the executable you want to have (e.g., `/cmd/myapp`).

Don't put a lot of code in the application directory. If you think the code can be imported and used in other projects, then it should live in the `/pkg` directory. If the code is not reusable or if you don't want others to reuse it, put that code in the `/internal` directory. You'll be surprised what others will do, so be explicit about your intentions!

It's common to have a small `main` function that imports and invokes the code from the `/internal` and `/pkg` directories and nothing else.

Examples:

* <https://github.com/heptio/ark/tree/master/cmd> (just a really small `main` function with everything else in packages)
* <https://github.com/moby/moby/tree/master/cmd>
* <https://github.com/prometheus/prometheus/tree/master/cmd>
* <https://github.com/influxdata/influxdb/tree/master/cmd>
* <https://github.com/kubernetes/kubernetes/tree/master/cmd>
* <https://github.com/dapr/dapr/tree/master/cmd>
* <https://github.com/ethereum/go-ethereum/tree/master/cmd>

Основные приложения проекта.

Имя директорий для каждого приложения должно совпадать с именем исполняемого файла, который вы хотите собрать (например, `/cmd/myapp`).

Не стоит располагать в этой директории большие объёмы кода. Если вы предполагает дальнейшее использование кода в других проектах, вам стоит хранить его в директории `/pkg` в корне проекта. Если же код не должен быть переиспользован где-то еще - ему самое место в директории `/internal` в корне проекта. Вы будете удивлены, что другие люди могут сделать, поэтому будьте уверены в своих намерениях!

Самой распространнёной практикой является использование маленькой `main` функции, которая импортирует и вызывает весь необходимый код из директорий `/internal` и `/pkg` и никаких других.

Ознакомьтесь с директорией [`/cmd`](cmd/README.md) для примера.
