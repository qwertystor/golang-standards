# `/website`

This is the place to put your project's website data if you are not using GitHub pages.

Examples:

* https://github.com/hashicorp/vault/tree/master/website
* https://github.com/perkeep/perkeep/tree/master/website

Здесь можно разделить файлы для вебсайта вашего проекта, если вы не используете `GitHub pages`.

Ознакомьтесь с директорией [`/website`](website/README.md) для примеров.