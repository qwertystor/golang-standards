# `/third_party`

External helper tools, forked code and other 3rd party utilities (e.g., Swagger UI).

Внешние вспомогательные инструменты, ответвления кода и другие сторонние утилиты (например, Swagger UI).
