# `/examples`

Examples for your applications and/or public libraries.

Examples:

* <https://github.com/nats-io/nats.go/tree/master/examples>
* <https://github.com/docker-slim/docker-slim/tree/master/examples>
* <https://github.com/gohugoio/hugo/tree/master/examples>
* <https://github.com/hashicorp/packer/tree/master/examples>

Примеры ваших приложений и/или библиотек.

Ознакомьтесь с директорией [`/examples`](examples/README.md) для примеров.
