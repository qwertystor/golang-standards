# `/internal`

Private application and library code. This is the code you don't want others importing in their applications or libraries. Note that this layout pattern is enforced by the Go compiler itself. See the Go 1.4 [`release notes`](https://golang.org/doc/go1.4#internalpackages) for more details. Note that you are not limited to the top level `internal` directory. You can have more than one `internal` directory at any level of your project tree.

You can optionally add a bit of extra structure to your internal packages to separate your shared and non-shared internal code. It's not required (especially for smaller projects), but it's nice to have visual clues showing the intended package use. Your actual application code can go in the `/internal/app` directory (e.g., `/internal/app/myapp`) and the code shared by those apps in the `/internal/pkg` directory (e.g., `/internal/pkg/myprivlib`).

Examples:

* <https://github.com/hashicorp/terraform/tree/master/internal>
* <https://github.com/influxdata/influxdb/tree/master/internal>
* <https://github.com/perkeep/perkeep/tree/master/internal>
* <https://github.com/jaegertracing/jaeger/tree/master/internal>
* <https://github.com/moby/moby/tree/master/internal>
* <https://github.com/satellity/satellity/tree/master/internal>

## `/internal/pkg`

Examples:

* <https://github.com/hashicorp/waypoint/tree/main/internal/pkg>

Внутренний код приложения и библиотек. Это код, который не должен быть применен в других приложениях и библиотеках. Стоит отметить, что этот шаблон навязан самим компилятором Golang. Ознакомьтесь с [`release notes`](https://golang.org/doc/go1.4#internalpackages)  Go 1.4. Также, вы вольны использовать `internal` директорию на разных уровнях своего проекта.

Вы можете добавить дополнительное структурирование, чтобы разделить открытую и закрытую части вашего внутреннего кода. Такой подход не является необходимым, особенно для маленьких проектов, но позволяет сразу визуально оценить применение кода. Код самого приложения может находиться в директории `/internal/app` (например, `/internal/app/myapp`) а код, который это приложение использует - в директории `/internal/pkg` (например, `/internal/pkg/myprivlib`).
