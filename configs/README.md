# `/configs`

Configuration file templates or default configs.

Put your `confd` or `consul-template` template files here.

Шаблоны файлов конфигураций и файлы настроек по-умолчанию.

Положите файлы конфигураций `confd` или `consul-template` сюда.
