# `/init`

System init (systemd, upstart, sysv) and process manager/supervisor (runit, supervisord) configs.

Файлы конфигураций для процессов инициализации системы (systemd, upstart, sysv) и менеджеров процессов (runit, supervisord).
