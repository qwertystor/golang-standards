# `/tools`

Supporting tools for this project. Note that these tools can import code from the `/pkg` and `/internal` directories.

Examples:

* <https://github.com/istio/istio/tree/master/tools>
* <https://github.com/openshift/origin/tree/master/tools>
* <https://github.com/dapr/dapr/tree/master/tools>

Инструменты поддержки проекта. Отметьте, что эти инструменты могут импортировать код из директорий `/pkg` и `/internal`.

Ознакомьтесь с директорией [`/tools`](tools/README.md) для примеров.
