# `/build`

Packaging and Continuous Integration.

Put your cloud (AMI), container (Docker), OS (deb, rpm, pkg) package configurations and scripts in the `/build/package` directory.

Put your CI (travis, circle, drone) configurations and scripts in the `/build/ci` directory. Note that some of the CI tools (e.g., Travis CI) are very picky about the location of their config files. Try putting the config files in the `/build/ci` directory linking them to the location where the CI tools expect them when possible (don't worry if it's not and if keeping those files in the root directory makes your life easier :-)).

Examples:

* <https://github.com/cockroachdb/cockroach/tree/master/build>

Упаковка и непрерывная интеграция.

Поместите файлы конфигурации и скрипты облака (AMI), контейнера (Docker), пакетов (deb, rpm, pkg) в директорию `/build/package`.

Поместите ваши файлы конфигурации CI (travis, circle, drone) и скрипты в директорию `/build/ci`. Отметьте, что некоторые инструменты CI (например, Travis CI) очень требовательны к расположению их конфигурационных файлов. Попробуйте поместить их в директорию `/build/ci` создав ссылку на них в месте, где их ожидают найти инструменты Go (когда возможно).
