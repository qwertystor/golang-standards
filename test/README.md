# `/test`

Additional external test apps and test data. Feel free to structure the `/test` directory anyway you want. For bigger projects it makes sense to have a data subdirectory. For example, you can have `/test/data` or `/test/testdata` if you need Go to ignore what's in that directory. Note that Go will also ignore directories or files that begin with "." or "_", so you have more flexibility in terms of how you name your test data directory.

Examples:

* https://github.com/openshift/origin/tree/master/test (test data is in the `/testdata` subdirectory)

Дополнительные внешние приложения и данные для тестирования. Вы вольны организовывать структуру директории `/test` так, как вам угодно. Для больших проектов имеет смысл создавать вложенную директорию с данными для тестов. Например,`/test/data` или `/test/testdata`, если вы хотите, чтобы Go игнорировал находящиеся там файлы. Отметьте, что Go будет также игнорировать файлы, путь к которым начинается с "." или "_", что предоставляет вам гибкость в наименовании вашей папки с тестами.

Ознакомьтесь с директорией [`/test`](test/README.md) для примеров.
