# `/vendor`

Application dependencies (managed manually or by your favorite dependency management tool like the new built-in, but still experimental, [`modules`](https://github.com/golang/go/wiki/Modules) feature).

Don't commit your application dependencies if you are building a library.

Note that since [`1.13`](https://golang.org/doc/go1.13#modules) Go also enabled the module proxy feature (using `https://proxy.golang.org` as their module proxy server by default). Read more about it [`here`](https://blog.golang.org/module-mirror-launch) to see if it fits all of your requirements and constraints. If it does, then you won't need the 'vendor' directory at all.

Зависимости приложений, управляемые вручную или с использованием вашей любимой системы управления зависимостями, вроде новых встроенных [`Go Modules`](https://github.com/golang/go/wiki/Modules)). Команда `go mod vendor` создаст для вас директорию `/vendor`. Заметьте, что вам возможно придётся добавить флаг `-mod=vendor` к команде `go build`, если вы используете версию, отличную от Go 1.14, где такой флаг выставлен по-умолчанию.

Не стоит отправлять зависимости вашего приложения в репозиторий, если собираетесь создавать библиотеку.

Стоит отметить, что с версии [`1.13`](https://golang.org/doc/go1.13#modules) Go добавил возможность проксирования модулей (с использованием [`https://proxy.golang.org`](https://proxy.golang.org) как прокси-сервера по-умолчанию). [`Здесь`](https://blog.golang.org/module-mirror-launch) можно побольше узнать про эту возможность, чтобы убедиться, что она удовлетворяет вашим необходимостям и ограничениям. Если это так - использование директории `vendor` не требуется вовсе.
